# Финансы

## История

```shell
http "http://13.75.91.36/api/finance/history/"
Authorization:"Token meowmeowmeow"
```

> Команда выше вернет JSON структурированный следющим образом:

```json
{
    "count": 54,
    "next": "http://13.75.91.36/api/finance/history/?page=2",
    "previous": null,
    "results": [
        {
            "id": 1,
            "date_time": "2017-03-01 00:00:00",
            "coins": 9.0,
            "currency": "HMQ",
            "from_user": 2,
            "to_user": 1,
            "bonus": false
        },
        {
            "id": 2,
            "date_time": "2017-03-05 00:00:00",
            "coins": 29.0,
            "currency": "HMQ",
            "from_user": null,
            "to_user": null,
            "bonus": true
        },
        ...
    ]
}
```

### HTTP Request

`GET http://13.75.91.36/api/finance/history/`

### Параметры запроса

Параметр | По умолчанию | Описание
--------- | ------- | -----------
page | 1 | Номер страницы

### Ответ

Параметр | Описание
--------- | -----------
id | Уникальный идентификатор
date_time | Дата и время перевода
coins | Сумма перевода
currency | Валюта
to_user | От кого произошол перевод
from_user | Кому перевод
bonus | true, если это бонус, при этом `to_user` и `from_user` будут равный `null`

## Перевод средств

```shell
http --form POST "http://13.75.91.36/api/finance/transfer/"
Authorization:"Token meowmeowmeow"
from_wallet=my_wallet_uuid
to_wallet=to_wallet_uuid
coins=50
```

> поле from_wallet не обязательно заполнять

```shell
http --form POST "http://13.75.91.36/api/finance/transfer/"
Authorization:"Token meowmeowmeow"
to_wallet=to_wallet_uuid
coins=50
```

> Метод вернет кошель, с которого списались средства с новым балансом:

```json
{
    "hash": "1b7121556e434026843150bacf5a13c8",
    "balance": 36.31,
    "currency": "HMQ",
    "blocked": false,
    "qr_code": "http://13.75.91.36/media/qr_codes/1b7121556e434026843150bacf5a13c8.png"
}
```

Метод производит перевод средств с кошелька `from_wallet` в `to_wallet` на сумму равную `coins`.
Поле `from_wallet` не обязательно заполнять, если поле будет пустое, то будет выбран активный
кошелек по умолчанию.

### Параметры запроса

Параметр | Тип | Описание
--------- | --- | -----------
from_wallet | Строка (UUID) | С какого кошелька снять средства (не обязательное)
to_wallet | Строка (UUID) | На какой кошелек перслать средства
coins | Float | Сумма перевода

### Ответ

Параметр | Описание
--------- | -----------
hash | Уникальный номер кошелька
balance | Баланс кошелька
currency | Валюта
blocked | `true`, если заблокирован
qr_code | Абволютный url к qr коду кошелька 

### Ошибки

Поле | Ошибка
--------- |-----------
to_wallet | Поле обязательно к заполнению
coins | Поле обязательно к заполнению
to_wallet | Укажите разные кошельки (from_wallet != to_wallet)
from_wallet | Это не ваш кошелек!
coins | У вас нет активного кошелька для снятия средств 
coins | Недостатьчно средств для списания

## Пополнить баланс кошелька

```shell
http --form POST "http://13.75.91.36/api/finance/add_coins/"
Authorization:"Token meowmeowmeow"
coins=50
```

Мтод пополняет баланс кошелька по умолчанию на сумму `coins`.

### HTTP Request

`GET http://13.75.91.36/api/finance/add_coins/`

### Параметры запроса

Параметр | Тип | Описание
--------- | ------- | -----------
coins | Float | Сумма пополнения

### Ответ

При успешном запросе возвращает статус код `200`

### Ошибки

Поле | Ошибка
--------- |-----------
coins | Поле обязательно к заполнению
coins | Это не число
