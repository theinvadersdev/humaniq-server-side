# Пользователи

## Список всех пользователей

```shell
http "http://13.75.91.36/api/user/list/"
Authorization:"Token meowmeowmeow"
```

> Команда выше вернет JSON структурированный следющим образом:

```json
{
    "count": 21,
    "next": "http://13.75.91.36/api/user/list/?page=3",
    "previous": "http://13.75.91.36/api/user/list/",
    "results": [
        {
            "id": 31,
            "register_date": "2017-03-01",
            "photo": "http://13.75.91.36/media/photos/photo.jpg",
            "wallet": {
                "hash": "1b7121556e434026843150bacf5a13c8",
                "balance": 36.31,
                "currency": "HMQ",
                "blocked": false,
                "qr_code": "http://13.75.91.36/media/qr_codes/1b7121556e434026843150bacf5a13c8.png"
            }
        },
        {
            "id": 32,
            "register_date": "2017-03-05",
            "photo": "http://13.75.91.36/media/photos/photo.jpg",
            "wallet": {
                "hash": "1b7121556e434026843150bacf5a13c8",
                "balance": 50.0,
                "currency": "HMQ",
                "blocked": false,
                "qr_code": "http://13.75.91.36/media/qr_codes/1b7121556e434026843150bacf5a13c8.png"
            }
        },
        ...
    ]
}
```

Метод возвращает список всех активных пользователей зарегистрированных в системе.

### HTTP Request

`GET http://13.75.91.36/api/user/list/`

### Параметры запроса

Параметр | По умолчанию | Описание
--------- | ------- | -----------
page | 1 | Номер страницы

### Ответ

Параметр | Описание
--------- | -----------
id | Уникальный идентификатор
register_date | Дата регистрации
photo | Абсолютный URL к фотографии пользователя

## Регистрация

```shell
http --form POST "http://13.75.91.36/api/user/register/"
photo="base64"
device_id="device_id"
```

> При успешной регистрации сервер вернет следующий JSON:

```json
{
    "token": "meowmeowmeow",
    "user": {
        "id": 44,
        "register_date": "2017-03-05",
        "photo": "http://13.75.91.36/media/photos/photo.jpg",
        "wallet": {
            "balance": 50.0,
            "hash": ""
        }
    }
}
```

Регистрация нового пользователя по лицу.

### HTTP Request

`POST http://13.75.91.36/api/user/register/`

### Параметры запроса

Параметр |  Описание
--------- |-----------
photo | Фотография пользователя закодированная в base64
device_id | Уникальный адрес телефона

### Ответ

Параметр | Описание
--------- | -----------
token | Access token для авторизации запроса
user | Модель пользователя

### Ошибки

Поле | Ошибка
--------- |-----------
photo | Поле обязательно к заполнению
photo | Изображение не вилидное
photo | Поле не в base64 кодировке
Нет | Ошибка при попытке извлечь дескриптор лица
Нет | Ошибка при попытке добавить пользователя в список
Нет | Пользователь с таким лицом уже есть в базе данных

## Авторизация

```shell
http --form POST "http://13.75.91.36/api/user/login/"
photo="base64"
device_id="device_id"
```

> При успешном входе сервер вернет следующий JSON:

```json
{
    "token": "meowmeowmeow",
    "user": {
        "id": 44,
        "register_date": "2017-03-05",
        "photo": "http://13.75.91.36/media/photos/photo.jpg",
        "wallet": {
            "balance": 50.0,
            "hash": ""
        }
    }
}
```

Вход в систему по лицу.

### HTTP Request

`POST http://13.75.91.36/api/user/login/`

### Параметры запроса

Параметр |  Описание
--------- |-----------
photo | Фотография пользователя закодированная в base64
device_id | Уникальный адрес телефона

### Ошибки

Поле | Ошибка
--------- |-----------
photo | Поле обязательно к заполнению
photo | Изображение не вилидное
photo | Поле не в base64 кодировке
Нет | Ошибка при попытке извлечь дескриптор лица
Нет | Ошибка при попытке добавить пользователя в список
