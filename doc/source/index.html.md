---
title: API Reference

language_tabs:
  - shell

toc_footers:
  - <a href='https://github.com/tripit/slate'>Documentation Powered by Slate</a>

includes:
  - users
  - finance
  - errors

search: true
---

# Авторизация запроса

Для авторизации необходимо добавить заголовок в запрос:

`Authorization: Token meowmeowmeow`

```shell
http "api_endpoint_here"
Authorization:"Token meowmeowmeow"
```

> Убедитесь, что вы заменили `meowmeowmeow` на полученный `access_token`.

<aside class="notice">
Вы должны заменить <code>meowmeowmeow</code> на <code>access_token</code>, который приходит при авторизации пользователя
</aside>

К каждому запросу необходимо добавлять этот заголовок, кроме `login` и `register`

# Пагинация

> При получении списка объектов они возвращаются не все сразу,
а разбиты на страницы следующим образом:

```json
{
    "count": 1000,
    "next": "next_endpoint_here",
    "previous": "previous_endpoint_here",
    "results": [
        {
            ...
        },
        {
            ...
        },
        ...
    ]
}
```

### Параметры запроса

Параметр | По умолчанию | Описание
--------- | ------- | -----------
page | 1 | Номер страницы

### Ответ

Параметр | Описание
--------- | -----------
count | Количество записей всего
next | Абсолютный URL к следующей странице
previous | Абсолютный URL к предыдущей странице
results | Текущая страница - массив из объектов

<aside class="notice">
поля <code>next</code> и <code>previous</code> могут быть равными <code>null</code>, это означате,
что это первая (если <code>previous = null</code>) или последняя (если <code>next = null</code>) страница.
</aside>
