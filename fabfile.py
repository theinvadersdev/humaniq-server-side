#!/usr/bin/python
# -*- coding: utf-8 -*-

import os.path
from fabric.api import local
from jinja2 import Template
import yaml


def test():
    local('virtualenv -p python3.5 .env')
    local('./.env/bin/pip install -r requirements.txt')

    local('mkdir -p var')
    local('mkdir -p var/static')
    local('mkdir -p var/media')
    local('mkdir -p var/media/photos')
    local('mkdir -p var/media/qr_codes')

    # Copy settings if not exist
    # if not os.path.isfile("src/humaniq/settings/local.py"):
    with open("ansible/roles/project/templates/settings_staging.j2", "r") as f:
        settings_template = Template(f.read())
        settings_template = settings_template.render(
            project_dir="",
            project_static_media_dir="",
            allowed_host="*",
            luna_vision_labs_server=os.environ["LUNA_VISION_LABS_API_SERVER"],
            luna_vision_labs_user=os.environ["LUNA_VISION_LABS_USER"],
            luna_vision_labs_passwd=os.environ["LUNA_VISION_LABS_PASSWD"],
            luna_vision_faces_list="test"
        )

    with open("src/humaniq/settings/local.py", "w") as f:
        f.write(settings_template)

    # manage.py
    def manage(cmd):
        local('./.env/bin/python src/manage.py {}'.format(cmd))

    manage('collectstatic --noinput')
    manage('migrate --noinput')

    manage('test luna.tests --noinput')
    manage('test humaniq.finance.tests --noinput')
    manage('test humaniq.api.user.tests --noinput')
    manage('test humaniq.api.finance.tests --noinput')

    local('rm -rf var')
    local('rm -rf ./.env')
