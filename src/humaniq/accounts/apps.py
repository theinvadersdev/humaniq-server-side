from django.apps import AppConfig


class AccountsConfig(AppConfig):
    name = 'humaniq.accounts'
