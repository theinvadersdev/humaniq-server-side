from django.conf import settings
from django.core.management import CommandError

import luna
import os
from django.core.management import BaseCommand


def load_face(file_name):
    with open(os.path.join(settings.STATIC_ROOT, "tests", file_name), "r") as f:
        return f.read()


class Command(BaseCommand):
    def handle(self, *args, **options):
        face = load_face("test_photo.txt")
        data = luna.data.TemplateInput(image=face)
        res = luna.api.templates(data, id=0)

        if res.status_code >= 400:
            raise CommandError('Failed add stub item. Status code %d' % res.status_code)
        else:
            self.stdout.write(self.style.SUCCESS('Successfully add stub descriptor with id = "0"'))

        res = luna.api.put_lists(id=settings.LUNA_VISION_LABS_FACES_LIST, descriptors=[0])

        if res.status_code >= 400:
            raise CommandError('Failed with status code %d' % res.status_code)
        else:
            self.stdout.write(self.style.SUCCESS('Successfully add list with id = "{}"'
                                                 .format(settings.LUNA_VISION_LABS_FACES_LIST)))
