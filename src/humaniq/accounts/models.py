import uuid

import luna
import base64
import os
from datetime import date

from rest_framework import status
from easy_thumbnails.fields import ThumbnailerImageField

from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.db import models
from django.conf import settings


class LunaAPIError(Exception):
    def __init__(self, response):
        self.response = response


class UserManager(BaseUserManager):
    def create(self, **kwargs):
        kwargs["username"] = str(uuid.uuid4())[:50]
        return super().create(**kwargs)

    def create_user(self, username, password=None):
        user = self.model(username=username)
        user.set_password(password)
        user.save(using=self._db)

        return user

    def create_superuser(self, username, password):
        user = self.create_user(
            username,
            password=password,
        )

        user.is_admin = True
        user.save(using=self._db)

        return user

    def get_descriptor_by_face_and_device_id(self, descriptor, device_id, exclude=None,
                                             faces_list=settings.LUNA_VISION_LABS_FACES_LIST,
                                             user_id=None):
        if user_id is not None:
            user_id = int(user_id)

        response = luna.api.similar_templates(id=descriptor.id, candidates_list=faces_list)

        if response.status_code != status.HTTP_200_OK:
            print(response.data.serialize())
            raise LunaAPIError(response)

        best_match = None
        match_user = None
        matches = response.data.matches

        for match in matches:
            if exclude is not None and match.id == exclude.id:
                continue

            if match.id == 0:
                continue

            user = User.objects.filter(id=match.id).first()

            if user is None:
                continue

            if user.device_id != device_id and user.device_id != "":
                continue

            if user_id is not None and user.id == user_id:
                match_user = user
                best_match = match
                break
            elif user_id is not None:
                print("U1 ", user_id)
                print("U2 ", user.id)
                continue

            if match_user is not None:
                if user.device_id != "" and match_user.device_id == "":
                    match_user = user
                    best_match = match
                    continue

            if best_match is None or best_match.similarity < match.similarity:
                # if best_match.
                match_user = user
                best_match = match

        if best_match is None:
            return None

        if best_match.similarity >= settings.FACE_SIMILARITY_THRESHOLD:
            if match_user.device_id == "":
                match_user.device_id = device_id
                match_user.save()

            return match_user

        return None


class User(AbstractBaseUser):
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = []

    username = models.CharField(editable=False, max_length=255, blank=True, unique=True)
    register_date = models.DateField(default=date.today)
    photo = ThumbnailerImageField("photo")
    device_id = models.CharField(max_length=255, default="")
    fcm_token = models.CharField(max_length=255, blank=True)
    apns_token = models.CharField(max_length=255, blank=True)
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)

    objects = UserManager()

    @property
    def default_wallet(self):
        if hasattr(self, "_default_wallet"):
            return self._default_wallet

        self._default_wallet = Wallet.active_objects.filter(user=self).first()
        return self._default_wallet

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    def delete(self, using=None, keep_parents=False):
        # response = luna.api.delete_template(self.id)
        #
        # if response.status_code != status.HTTP_200_OK:
        #     print(response.data.serialize())
        #     raise LunaAPIError(response)

        photo_path = os.path.join(settings.MEDIA_ROOT, self.photo.path)
        os.remove(photo_path)
        return super().delete(using, keep_parents)

    def get_short_name(self):
        return self.username

    @property
    def photo_url(self):
        photo_im = self.photo['photo']
        photo_url = photo_im.url
        return photo_url

    @property
    def is_staff(self):
        return self.is_admin

    @property
    def base64_photo(self):
        path = os.path.join(settings.MEDIA_ROOT, self.photo.path)
        with open(path, "rb") as image_file:
            return base64.b64encode(image_file.read()).decode('ascii')


from humaniq.finance.models import Wallet, Currency
