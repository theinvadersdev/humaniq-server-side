import base64
import six
import uuid
import os

from django.core.files.base import ContentFile
from django.conf import settings
from rest_framework import serializers


def get_base64_photo(file, base64=None):
    if base64 is not None:
        return base64

    return base64.b64encode(file.read()).decode('ascii')


class Base64ImageMixin(object):
    to_upload = "."

    def decode_data(self, data):
        if isinstance(data, six.string_types):
            if 'data:' in data and ';base64,' in data:
                header, data = data.split(';base64,')
            try:
                decoded_file = base64.b64decode(data)
            except:
                self.fail('invalid_image')

            file_name = str(uuid.uuid4())
            file_extension = self.get_file_extension(file_name, decoded_file)
            complete_file_name = "%s.%s" % (self.to_upload+"/"+file_name, file_extension)

            fh = open(os.path.join(settings.MEDIA_ROOT, complete_file_name), "wb")
            fh.write(decoded_file)
            fh.close()

            data = ContentFile(decoded_file, name=complete_file_name)

        return data

    @staticmethod
    def get_file_extension(file_name, decoded_file):
        import imghdr

        extension = imghdr.what(file_name, decoded_file)
        extension = "jpg" if extension == "jpeg" else extension

        return extension


class Base64ImageField(Base64ImageMixin, serializers.ImageField):
    to_upload = "photos"

    def to_internal_value(self, data):
        data = self.decode_data(data)
        return super(Base64ImageField, self).to_internal_value(data)
