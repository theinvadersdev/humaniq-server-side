from rest_framework.exceptions import ValidationError


class FieldValidationError(ValidationError):
    def __init__(self, field, detail, code=None):
        detail = {field: [detail]}
        super().__init__(detail, code)
