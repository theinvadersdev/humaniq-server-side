import uuid

from django.db import transaction
from rest_framework import serializers

from humaniq.finance.models import HistoryItem, Wallet


class HistorySerializer(serializers.ModelSerializer):
    currency = serializers.SerializerMethodField()
    from_user = serializers.SerializerMethodField()
    to_user = serializers.SerializerMethodField()
    from_address = serializers.SerializerMethodField()
    to_address = serializers.SerializerMethodField()
    bonus = serializers.SerializerMethodField()

    def get_currency(self, history_item):
        return history_item.currency.name

    def get_from_user(self, history_item):
        if history_item.from_wallet is None:
            return None

        return history_item.from_wallet.user.id

    def get_to_user(self, history_item):
        if history_item.to_wallet is None:
            return None

        return history_item.to_wallet.user.id

    def get_bonus(self, history_item):
        return history_item.bonus is not None

    def get_from_address(self, history_item):
        if history_item.from_wallet is None:
            return None

        return history_item.from_wallet.hash

    def get_to_address(self, history_item):
        if history_item.to_wallet is None:
            return None

        return history_item.to_wallet.hash

    class Meta:
        model = HistoryItem
        fields = (
            "id",
            "coins",
            "bonus",
            "currency",
            "date_time",
            "from_user",
            "to_user",
            "from_address",
            "to_address",
        )


class WalletSerializer(serializers.ModelSerializer):
    currency = serializers.SerializerMethodField()

    def get_currency(self, wallet):
        return wallet.currency.name

    class Meta:
        model = Wallet
        fields = (
            "hash",
            "balance",
            "blocked",
            "user",
            "currency",
            "qr_code"
        )


class TransferSerializer(serializers.Serializer):
    to_wallet = serializers.PrimaryKeyRelatedField(
        queryset=Wallet.active_objects.all(),
        required=True,
    )
    from_wallet = serializers.PrimaryKeyRelatedField(
        queryset=Wallet.active_objects.all(),
        required=False,
    )
    coins = serializers.FloatField(
        required=True,
        min_value=1
    )

    class Meta:
        fields = (
            'to_wallet',
            'from_wallet',
            'coins',
        )

    def _get_from_wallet(self, user):
        wallet = self.initial_data.get("from_wallet", user.default_wallet)

        if wallet != user.default_wallet:
            wallet = Wallet.active_objects.filter(hash=wallet).first()

        return wallet

    def validate_to_wallet(self, to_wallet):
        user = self.context["request"].user
        from_wallet = self._get_from_wallet(user)

        if to_wallet == from_wallet:
            raise serializers.ValidationError("Specify different wallets for transfer")

        return to_wallet

    def validate_from_wallet(self, from_wallet):
        user = self.context["request"].user

        if from_wallet.user != user:
            raise serializers.ValidationError("It's not your wallet!")

        return from_wallet

    def validate_coins(self, value):
        user = self.context["request"].user
        wallet = self._get_from_wallet(user)

        if wallet is None:
            raise serializers.ValidationError("You don't have active wallets")

        if wallet.balance < value:
            raise serializers.ValidationError("Not enough coins for debiting")

        return value
