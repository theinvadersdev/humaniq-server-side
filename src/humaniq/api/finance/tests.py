from rest_framework import status
from rest_framework.test import APITestCase

from humaniq.accounts.models import User
from humaniq.finance.models import Wallet, Currency, HistoryItem


class FinanceTestCase(APITestCase):
    def setUp(self):
        self.user1 = User.objects.create(username="user1")
        self.user2 = User.objects.create(username="user2")

        self.hmq_currency = Currency.objects.create(name="HMQ", code="hmq")

        self.wallet1 = Wallet.objects.create(user=self.user1, balance=100,
                                             currency=self.hmq_currency)
        self.wallet2 = Wallet.objects.create(user=self.user2, balance=100,
                                             currency=self.hmq_currency)
        print(self.wallet2.qr_code)

    def test_history(self):
        self.client.force_login(self.user1)
        HistoryItem.objects.create(from_wallet=self.wallet1, to_wallet=self.wallet2, coins=50,
                                   currency=self.wallet1.currency)
        res = self.client.get("/api/finance/history/")
        self.assertEqual(res.status_code, status.HTTP_200_OK)

        self.assertEqual(res.json()["results"][0]["coins"], 50.0)
        self.assertEqual(res.json()["results"][0]["currency"], "HMQ")
        self.assertEqual(res.json()["results"][0]["from_user"], 1)
        self.assertEqual(res.json()["results"][0]["to_user"], 2)

    def test_transfer(self):
        self.client.force_login(self.user1)

        # Не все поля заполенены
        data = {
            "to_wallet": self.wallet2.hash,
        }

        res = self.client.post("/api/finance/transfer/", data)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)

        # На свой кошелек нельзя переводить
        data = {
            "from_wallet": self.wallet1.hash,
            "to_wallet": self.wallet1.hash,
            "coins": 50.0
        }

        res = self.client.post("/api/finance/transfer/", data)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)

        # Недостаточно средств для перевода такой суммы
        data = {
            "to_wallet": self.wallet2.hash,
            "coins": 101.0
        }

        res = self.client.post("/api/finance/transfer/", data)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)

        # Попытка перевести деньги с чужого кошелька!
        data = {
            "from_wallet": self.wallet2.hash,
            "to_wallet": self.wallet1.hash,
            "coins": 50.0
        }

        res = self.client.post("/api/finance/transfer/", data)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)

        # Попытка перевести 0 монет
        data = {
            "to_wallet": self.wallet2.hash,
            "coins": 0.0
        }

        res = self.client.post("/api/finance/transfer/", data)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)

        # Успешный перевод средств
        data = {
            "to_wallet": self.wallet2.hash,
            "coins": 50.0
        }

        res = self.client.post("/api/finance/transfer/", data)
        self.assertEqual(res.status_code, status.HTTP_200_OK)

        print(res.json())

        self.wallet1.refresh_from_db()
        self.wallet2.refresh_from_db()

        bonus = 20  # Разовый бонус за отправку

        self.assertEqual(self.wallet1.balance, 50.0+bonus)
        self.assertEqual(self.wallet2.balance, 150.0+bonus)

        # Проверка истории
        history_item = HistoryItem.objects.all()[0]

        self.assertIsNotNone(history_item)
        self.assertEqual(history_item.from_wallet, self.user1.default_wallet)
        self.assertEqual(history_item.to_wallet, self.user2.default_wallet)
        self.assertEqual(history_item.coins, 50.0)
        self.assertEqual(history_item.currency, self.wallet1.currency)

        # Проверка бонусов в истории
        history_item = HistoryItem.objects.all()[1]

        self.assertIsNotNone(history_item)
        self.assertEqual(history_item.bonus.user, self.user1)
        self.assertEqual(history_item.coins, 20.0)

        history_item = HistoryItem.objects.all()[2]

        self.assertIsNotNone(history_item)
        self.assertEqual(history_item.bonus.user, self.user2)
        self.assertEqual(history_item.coins, 20.0)

        # Следующий перевод должен быть без бонусов
        data = {
            "to_wallet": self.wallet2.hash,
            "coins": 50.0
        }

        res = self.client.post("/api/finance/transfer/", data)
        self.assertEqual(res.status_code, status.HTTP_200_OK)

        self.wallet1.refresh_from_db()
        self.wallet2.refresh_from_db()

        self.assertEqual(self.wallet1.balance, 20.0)  # Был остаток 70 с учетом бонусов
        self.assertEqual(self.wallet2.balance, 220.0)

        # Бонус на отправку с другого пользователя
        self.client.force_login(self.user2)

        data = {
            "to_wallet": self.wallet1.hash,
            "coins": 50.0
        }

        res = self.client.post("/api/finance/transfer/", data)
        self.assertEqual(res.status_code, status.HTTP_200_OK)

        self.wallet1.refresh_from_db()
        self.wallet2.refresh_from_db()

        self.assertEqual(self.wallet1.balance, 70.0 + bonus)  # Дополнительный бонус за принятие
        self.assertEqual(self.wallet2.balance, 170.0 + bonus) # Бонус за отправку

        # Поторное отправление уже без бонусов
        self.client.force_login(self.user2)

        data = {
            "to_wallet": self.wallet1.hash,
            "coins": 50.0
        }

        res = self.client.post("/api/finance/transfer/", data)
        self.assertEqual(res.status_code, status.HTTP_200_OK)

        self.wallet1.refresh_from_db()
        self.wallet2.refresh_from_db()

        self.assertEqual(self.wallet1.balance, 140.0)
        self.assertEqual(self.wallet2.balance, 140.0)
