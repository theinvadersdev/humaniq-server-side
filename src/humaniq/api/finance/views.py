from django.db import transaction
from django.db.models import Q
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from humaniq.api.exceptions import FieldValidationError
from humaniq.api.finance.serializers import HistorySerializer, TransferSerializer, WalletSerializer
from humaniq.api.response import pagination_response
from humaniq.finance.models import HistoryItem, Bonus, Currency


@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def history(request):
    user = request.user
    query = Q(from_wallet__user=user) | Q(to_wallet__user=user) | Q(bonus__user=user)
    items = HistoryItem.objects.filter(query).order_by("-date_time")
    return pagination_response(request, items, HistorySerializer)


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def transfer(request):
    serializer = TransferSerializer(data=request.data, context={"request": request})

    if not serializer.is_valid():
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    user = request.user
    to_wallet = serializer.validated_data["to_wallet"]
    from_wallet = serializer.validated_data.get("from_wallet", user.default_wallet)
    coins = serializer.validated_data["coins"]

    # TODO: Вынести метод за пределы `transfer`
    def send_bonus(wallet, code):
        """
        Отправка разового бонуса. Отправляет бонус, если еше ни разу не отправлялся до этого.
        """
        to_user = wallet.user

        if Bonus.objects.filter(user=to_user, code=code).count() > 0:
            return

        value, currency_code = Bonus.BonusValues.NEWBIE
        currency = Currency.objects.get(code=currency_code)
        bonus = Bonus.objects.create(user=to_user, code=code, value=value, currency=currency)
        wallet.replenish(value=value, currency=currency)

        HistoryItem.objects.create(bonus=bonus, coins=bonus.value, currency=bonus.currency)

    # Перевод средств
    with transaction.atomic():
        from_wallet.withdraw(coins, commit=False)
        to_wallet.replenish(coins, commit=False)

        from_wallet.save()
        to_wallet.save()

        HistoryItem.objects.create(from_wallet=from_wallet, to_wallet=to_wallet, coins=coins,
                                   currency=from_wallet.currency)

        send_bonus(from_wallet, "newbie_transfer")
        send_bonus(to_wallet, "newbie_receive")

    wallet_serializer = WalletSerializer(instance=from_wallet)
    return Response(data=wallet_serializer.data, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def add_coins(request):
    if request.POST.get("coins") is None:
        raise FieldValidationError("coins", "Required field")

    try:
        request.user.default_wallet.balance += float(request.POST.get("coins"))
        request.user.default_wallet.save()
    except ValueError:
        raise FieldValidationError("coins", "Not a number")

    return Response(status=status.HTTP_200_OK)
