from rest_framework.pagination import PageNumberPagination


def pagination_response(request, qs, serializer_class, page_size=100):
    paginator = PageNumberPagination()
    paginator.page_size = page_size
    result_page = paginator.paginate_queryset(qs, request)

    serializer = serializer_class(result_page, many=True)
    return paginator.get_paginated_response(serializer.data)
