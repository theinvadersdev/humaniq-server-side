import mimetypes
import uuid
import os

from django.conf import settings
from django.core.files import File


def generate_filename(path, ext):
    def generate():
        return os.path.join(settings.MEDIA_ROOT, path, str(uuid.uuid4()) + ext)

    file_name = generate()

    while os.path.isfile(file_name):
        file_name = generate()

    return file_name


def upload_multipart_file(request, field, upload_path="", kls=File):
    file = request.FILES.get(field)

    if file is None:
        return None

    file = request.FILES.get(field)
    mime = file.content_type
    ext = mimetypes.guess_extension(mime)

    file = kls(file)
    file.name = generate_filename(upload_path, ext)

    return file


def upload_multipart_file_set(request, field, upload_path="", kls=File):
    files = request.FILES.getlist(field)
    files_set = []

    if files is None:
        return None

    for file in files:
        mime = file.content_type
        ext = mimetypes.guess_extension(mime)

        file = kls(file)
        file.name = generate_filename(upload_path, ext)

        files_set.append(file)

    return files_set
