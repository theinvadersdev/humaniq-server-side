from django.conf.urls import url

from rest_framework.urlpatterns import format_suffix_patterns

from humaniq.api.user import views as user_views
from humaniq.api.finance import views as finance_views


urlpatterns = format_suffix_patterns([
    url(r'^user/my/$', user_views.my),
    url(r'^user/get_by_id/$', user_views.get_by_id),
    url(r'^user/list/$', user_views.list),
    url(r'^user/login/$', user_views.login),
    url(r'^user/logout/$', user_views.logout),
    url(r'^user/register/$', user_views.register),

    url(r'^finance/history/$', finance_views.history),
    url(r'^finance/transfer/$', finance_views.transfer),
    url(r'^finance/add_coins/$', finance_views.add_coins),
])
