from rest_framework import serializers

from humaniq.accounts.models import User
from humaniq.api.base64_image_field import Base64ImageField
from humaniq.api.finance.serializers import WalletSerializer


class RegisterSerializer(serializers.ModelSerializer):
    photo = Base64ImageField(
        max_length=None,
        use_url=True,
        required=False
    )
    device_id = serializers.CharField(default="", required=False)

    class Meta:
        model = User
        fields = (
            "id",
            "register_date",
            "fcm_token",
            "apns_token",
            "device_id",
            "photo",
        )
        read_only_fields = (
            "photo",
            "register_date"
        )
        extra_kwargs = {
            "apns_token": {'write_only': True},
            "fcm_token": {'write_only': True},
            "photo": {'write_only': True},
        }


class LoginSerializer(serializers.Serializer):
    photo = Base64ImageField(
        max_length=None,
        use_url=True,
        required=False
    )
    device_id = serializers.CharField(default="", required=False)

    class Meta:
        fields = (
            "photo",
            "device_id",
        )


class UserSerializer(serializers.ModelSerializer):
    photo = serializers.SerializerMethodField()
    wallet = serializers.SerializerMethodField()

    def get_photo(self, user):
        return user.photo_url

    def get_wallet(self, user):
        return WalletSerializer(instance=user.default_wallet).data

    class Meta:
        model = User
        fields = (
            "id",
            "register_date",
            "photo",
            "wallet",
        )
