import os

from django.conf import settings

from rest_framework import status
from rest_framework.test import APITestCase

import luna
from humaniq.accounts.models import User


def load_face(file_name):
    with open(os.path.join(settings.STATIC_ROOT, "tests", file_name), "r") as f:
        return f.read()


class UserTestCase(APITestCase):
    def test_register_and_login(self):
        data = {
            "photo": load_face("test_photo.txt"),
            "device_id": "device_1"
        }

        res = luna.api.put_lists(id="test", descriptors=[0])
        self.assertEqual(res.status_code, 200)

        res = self.client.post("/api/user/register/?test_list=true", data)
        print(res.json())
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        print("REGISTRATION PASSED")
        print(res.json())

        res = self.client.post("/api/user/login/?test_list=true", data)
        self.assertEqual(res.status_code, status.HTTP_200_OK)

        print("LOGIN PASSED")
        print(res.json())

        user_1 = User.objects.get(id=res.json()["user"]["id"])
        self.assertEqual(user_1.device_id, "device_1")

        data["device_id"] = "device_2"
        res = self.client.post("/api/user/login/?test_list=true", data)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)

        print("LOGIN WITH BAD DEVICE_ID PASSED")
        print(res.json())

        # Проверка обратной совместимости
        data = {
            "photo": load_face("test_photo.txt"),
        }

        res = self.client.post("/api/user/register/?test_list=true", data)
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        print("REGISTRATION WITHOUT DEVICE_ID PASSED")
        print(res.json())

        res = self.client.post("/api/user/login/?test_list=true", data)
        self.assertEqual(res.status_code, status.HTTP_200_OK)

        print("LOGIN WITHOUT DEVICE_ID PASSED")
        print(res.json())

        # Проверка заполнения пустых device_id
        data = {
            "photo": load_face("test_photo.txt"),
            "device_id": "test"
        }

        res = self.client.post("/api/user/login/?test_list=true", data)
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        print(res.json())

        user_2 = User.objects.get(id=res.json()["user"]["id"])
        self.assertEqual(user_2.device_id, "test")
        user_1.refresh_from_db()
        self.assertEqual(user_1.device_id, "device_1")

        data["device_id"] = ""
        res = self.client.post("/api/user/login/?test_list=true", data)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)

        # User id
        data["device_id"] = "test"
        data["user_id"] = user_2.id

        res = self.client.post("/api/user/register/?test_list=true", data)
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)
        self.assertEqual(res.json()["user"]["id"], user_2.id)

        data["device_id"] = "test"
        res = self.client.post("/api/user/register/?test_list=true", data)
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)
        self.assertEqual(res.json()["user"]["id"], user_2.id)
