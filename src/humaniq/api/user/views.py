import copy

import luna
import base64
import os

from django.core.files.images import ImageFile
from django.utils.translation import ugettext as _
from django.conf import settings

from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from rest_framework.exceptions import ValidationError
from rest_framework.permissions import IsAuthenticated

from humaniq.accounts.models import User, LunaAPIError
from humaniq.api.response import pagination_response
from humaniq.api.upload import upload_multipart_file
from humaniq.api.user.serializers import RegisterSerializer, UserSerializer, LoginSerializer
from humaniq.finance.models import Wallet, Currency


@api_view(["GET"])
# @permission_classes((IsAuthenticated,))
def list(request):
    users = User.objects.all()
    return pagination_response(request, users, UserSerializer)


@api_view(["GET"])
@permission_classes((IsAuthenticated,))
def my(request):
    return Response(UserSerializer(request.user).data, status=status.HTTP_200_OK)


@api_view(["GET"])
@permission_classes((IsAuthenticated,))
def get_by_id(request):
    user = User.objects.get(id=request.GET.get("id"))
    return Response(UserSerializer(user).data, status=status.HTTP_200_OK)


@api_view(["POST"])
def login(request):
    user_id = request.POST.get("user_id")
    is_test_list = request.GET.get("test_list", "false")
    is_test_list = is_test_list == "true" or is_test_list == "1"

    file = upload_multipart_file(request, field="photo", upload_path="photos", kls=ImageFile)

    if file is None and request.POST.get("photo") is None:
        raise ValidationError(_("Please upload photo"))

    serializer = LoginSerializer(data=request.data)

    # TODO: Вынести часть кода в метод
    if not serializer.is_valid():
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    photo = serializer.validated_data.get("photo")
    photo_path = os.path.join(settings.MEDIA_ROOT, photo.name)

    with open(photo_path, "rb") as image_file:
        base64_photo = base64.b64encode(image_file.read()).decode('ascii')

    os.remove(photo_path)

    # Извлечение дескриптора из фотографии
    data = luna.data.TemplateInput(image=base64_photo)
    response = luna.api.templates(data, id=-1, temporary=False)
    descriptor = response.data

    if response.status_code != luna.api.TEMPLATES_OK_STATUS_CODE:
        print(response.data.serialize())
        raise ValidationError(response.data.detail)

    # Поиск пользователя по извлеченному дескрпитору
    try:
        device_id = serializer.validated_data.get("device_id")
        user_faces_list = "test" if is_test_list else settings.LUNA_VISION_LABS_FACES_LIST
        user = User.objects.get_descriptor_by_face_and_device_id(descriptor, device_id,
                                                                 faces_list=user_faces_list,
                                                                 user_id=user_id)
        if user is None:
            raise ValidationError(_("User with this face doesn't exist"))
    except LunaAPIError:
        print(response.data.serialize())
        raise ValidationError(_("Fail, when trying to validate face descriptor"))

    # Создание токена для доступа
    Token.objects.filter(user=user).delete()
    token = Token.objects.create(user=user)

    return Response({
        "token": token.key,
        "user": UserSerializer(instance=user).data
    })


@api_view(["POST"])
def logout(request):
    if request.user.is_anonymous:
        return Response(status=status.HTTP_200_OK)

    # Token.objects.filter(user=request.user).delete()
    return Response(status=status.HTTP_200_OK)


@api_view(["POST"])
def register(request):
    user_id = request.POST.get("user_id")
    serializer = RegisterSerializer(data=request.data)
    is_test_list = request.GET.get("test_list", "false")
    is_test_list = is_test_list == "true" or is_test_list == "1"

    if not serializer.is_valid():
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    file = upload_multipart_file(request, field="photo", upload_path="photos", kls=ImageFile)

    if file is None and serializer.validated_data.get("photo") is None:
        raise ValidationError(_("Please upload photo"))

    user = serializer.save()

    # Извлечение дескриптора из фотографии
    data = luna.data.TemplateInput(image=user.base64_photo)
    response = luna.api.templates(data, id=user.id, temporary=False)

    if response.status_code != luna.api.TEMPLATES_OK_STATUS_CODE:
        user.delete()
        print(response.data.serialize())
        raise ValidationError(response.data.detail)

    descriptor = response.data

    def token_response(token_user, status=status.HTTP_201_CREATED):
        """
        Создание токена для доступа.
        :return успешный ответ с токеном
        """
        if token_user is None:
            raise User.DoesNotExist

        Token.objects.filter(user=token_user).delete()
        token = Token.objects.create(user=token_user)

        return Response({
            "token": token.key,
            "user": UserSerializer(instance=token_user).data
        }, status=status)

    # Проверка, есть ли такое лицо уже в базе данные
    try:
        device_id = serializer.validated_data.get("device_id")
        user_faces_list = "test" if is_test_list else settings.LUNA_VISION_LABS_FACES_LIST
        found_user = User.objects.get_descriptor_by_face_and_device_id(descriptor, device_id,
                                                                       faces_list=user_faces_list,
                                                                       user_id=user_id)

        if found_user is not None:
            user.delete()
            # return token_response(found_user, status.HTTP_200_OK)
            return token_response(found_user)
        else:
            if user_id is not None:
                raise ValidationError(_("Sorry"))
            # raise ValidationError(_("User with this face already exist"))
    except LunaAPIError:
        user.delete()
        print(response.data.serialize())
        raise ValidationError(_("Fail, when trying to validate face descriptor"))

    # Добавление нового дескриптора в список `user_faces_list` для сравнения
    response = luna.api.put_lists(id=user_faces_list, descriptors=[user.id], mode="patch")

    if response.status_code != status.HTTP_200_OK:
        user.delete()
        print(response.data.serialize())
        raise ValidationError(_("Fail, when trying to add face to list"))

    # Создание кошелька
    hmq_currency, created = Currency.objects.get_or_create(code="hmq", name="HMQ")

    wallet = Wallet(user=user, currency=hmq_currency, balance=100.0)
    wallet.generate_qr_code()
    wallet.save()

    return token_response(user)
