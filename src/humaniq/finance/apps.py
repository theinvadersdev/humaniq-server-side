from django.apps import AppConfig


class FinanceConfig(AppConfig):
    name = 'humaniq.finance'
