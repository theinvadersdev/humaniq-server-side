from django.core.management import BaseCommand

from humaniq.finance.models import Wallet


class Command(BaseCommand):
    def handle(self, *args, **options):
        for wallet in Wallet.objects.all():
            wallet.generate_qr_code()
            wallet.save()

        self.stdout.write(self.style.SUCCESS('Successfully'))
