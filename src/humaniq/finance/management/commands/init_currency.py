from django.core.management import BaseCommand

from humaniq.accounts.models import User
from humaniq.finance.models import Currency, Wallet


class Command(BaseCommand):
    def handle(self, *args, **options):
        hmq_currency = Currency.objects.get_or_create(code="hmq", name="HMQ")
        hmq_currency = hmq_currency[0]

        # Создание кощельков для пользоваетелй, если у них их нет
        for user in User.objects.all():
            if len(user.wallets.all()) == 0:
                Wallet.objects.create(user=user, currency=hmq_currency)

        self.stdout.write(self.style.SUCCESS('Successfully'))
