from uuid import uuid4
import qrcode
import io
import os

from datetime import datetime

from django.conf import settings
from django.core.files.base import ContentFile
from django.db import models
from qrcode import ERROR_CORRECT_L
from qrcode import ERROR_CORRECT_M
from qrcode import QRCode

from humaniq.accounts.models import User


class Currency(models.Model):
    name = models.CharField(max_length=100)
    code = models.CharField(max_length=10)

    def convert_to_currency(self, currency, value):
        return value


class ActiveWalletsManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(blocked=False)


class WalletObjectManager(models.Manager):
    def create(self, **kwargs):
        obj = super().create(**kwargs)
        obj.generate_qr_code()
        return obj


class Wallet(models.Model):
    class Meta:
        default_related_name = "wallets"

    user = models.ForeignKey(User)
    hash = models.UUIDField(primary_key=True, default=uuid4, unique=True, editable=False)
    balance = models.FloatField(default=0)
    blocked = models.BooleanField(default=False)
    currency = models.ForeignKey(Currency, null=True)
    qr_code = models.ImageField(blank=True, upload_to="qr_codes/")

    objects = WalletObjectManager()
    active_objects = ActiveWalletsManager()

    def replenish(self, value, currency="hmq", commit=True):
        if isinstance(currency, str):
            currency = Currency.objects.get(code=currency)

        self.balance += self.currency.convert_to_currency(currency, value)

        if commit:
            self.save()

    def withdraw(self, value, currency="hmq", commit=True):
        if isinstance(currency, str):
            currency = Currency.objects.get(code=currency)

        self.balance -= self.currency.convert_to_currency(currency, value)

        if commit:
            self.save()

    def generate_qr_code(self):
        qr_codes_dir = os.path.join(settings.MEDIA_ROOT, "qr_codes/")

        if not os.path.exists(qr_codes_dir):
            os.makedirs(qr_codes_dir)

        qr = QRCode(version=5, error_correction=ERROR_CORRECT_M)
        qr.add_data(str(self.hash))
        qr.make()

        im = qr.make_image()

        filename = str(self.hash)+".png"
        abs_path = os.path.join(qr_codes_dir, filename)
        rel_path = "qr_codes/" + filename

        im.save(abs_path)
        self.qr_code = rel_path


class Bonus(models.Model):
    class BonusValues:
        NEWBIE = (20, "hmq")

    user = models.ForeignKey(User)
    code = models.CharField(blank=True, max_length=60)
    currency = models.ForeignKey(Currency, null=True)
    value = models.FloatField(default=0)


class HistoryItem(models.Model):
    class Meta:
        default_related_name = "history"

    from_wallet = models.ForeignKey(Wallet, related_name="from_wallet", null=True)
    to_wallet = models.ForeignKey(Wallet, related_name="to_wallet", null=True)
    bonus = models.ForeignKey(Bonus, null=True)
    date_time = models.DateTimeField(default=datetime.now)
    coins = models.FloatField(default=0)
    currency = models.ForeignKey(Currency)
