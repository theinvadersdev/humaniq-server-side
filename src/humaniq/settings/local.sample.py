from humaniq.settings.base import *
import raven

VAR_ROOT = os.path.join(BASE_DIR, '../../var')
STATIC_ROOT = os.path.join(VAR_ROOT, 'static')
MEDIA_ROOT = os.path.join(VAR_ROOT, 'media')

DEBUG = True
SECRET_KEY = 'demo'

ALLOWED_HOSTS = [
    "*"
]

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

STATIC_URL = '/static/'
MEDIA_URL = '/media/'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(VAR_ROOT, 'db.sqlite3'),
    }
}

LOCALE_PATHS = (
    os.path.join(VAR_ROOT, 'locale'),
)

LUNA_VISION_LABS_MASTER_MODE = False
LUNA_VISION_LABS_API_SERVER = ""

LUNA_VISION_LABS_USER = ""
LUNA_VISION_LABS_PASSWD = ""
LUNA_VISION_LABS_VERSION = "5"
LUNA_VISION_LABS_FACES_LIST = ""

TEST_RUNNER = "django.test.runner.DiscoverRunner"

RAVEN_CONFIG = {
    "dsn": "",
    "release": raven.fetch_git_sha(ROOT_DIR),
}
