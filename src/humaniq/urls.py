from django.conf import settings
from django.conf.urls import url, include
from django.contrib import admin
from django.conf.urls.static import static


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api/', include('humaniq.api.urls', namespace="api")),
]

if settings.DEBUG:
    # urlpatterns += [
    #     url(r'^layout/(?P<template>[A-z0-9_\-.]+)/$', views.LayoutView.as_view())
    # ]

    urlpatterns += static("media/", document_root=settings.MEDIA_ROOT)
