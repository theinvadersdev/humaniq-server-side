from luna.api_helpers import *
from luna.data import *


def version():
    return requests.get(
        get_server_url("version"),
        auth=get_auth()
    )


TEMPLATES_OK_STATUS_CODE = 200 if settings.LUNA_VISION_LABS_MASTER_MODE else 201


def templates(data, id=None, temporary=None):
    """Извлечение дескриптор лица из изображения. Синхронный запрос"""
    url = build_url("templates", id=id, temporary=temporary)
    method = "POST" if settings.LUNA_VISION_LABS_MASTER_MODE else "PUT"
    response = make_request(method, url, data)
    return Result(response, TemplateOutput)


def delete_template(id):
    """Удаление дескриптора лица"""
    url = build_url("templates", id=id)
    response = make_request("DELETE", url)
    return Result(response, TemplateOutput)


def extractor(data, id=None, temporary=None):
    """Извлечение дескриптор лица из изображения. Асинхронный запрос"""
    url = build_url("extractor", id=id, temporary=temporary)
    response = make_request("POST", url, data)
    return Result(response, AsyncOutput)


def similar_templates(id, candidates=None, candidates_list=None, candidates_range=None,
                      temporary=None, limit=None):
    """Сопоставление дескрипторов Синхронный запрос"""
    url = build_url("similar_templates", id=id, candidates=candidates,
                    candidates_list=candidates_list, candidates_range=candidates_range,
                    temporary=temporary, limit=limit)
    response = make_request("GET", url)
    return Result(response, SimilarTemplates)


def put_lists(id, descriptors, mode="put"):
    """Добавление (пополнение) списка"""
    url = build_url("lists", id=id)
    data = {
        "data": descriptors,
        "mode": mode
    }
    response = make_request("PUT", url, data)
    return Result(response)
