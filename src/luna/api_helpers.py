import requests
from django.conf import settings

from luna.serializer import Serializable


def get_auth():
    return settings.LUNA_VISION_LABS_USER, settings.LUNA_VISION_LABS_PASSWD


def get_server_url(path):
    if path == "version":
        return "{url}/version".format(
            url=settings.LUNA_VISION_LABS_API_SERVER
        )

    return "{url}/{version}/{path}".format(
        url=settings.LUNA_VISION_LABS_API_SERVER,
        version=settings.LUNA_VISION_LABS_VERSION,
        path=path
    )


def build_url(path, **kwargs):
    data = []

    def val(v):
        if type(v) is bool:
            return "1" if v else "0"

        return v

    for key in kwargs:
        if kwargs[key] is not None:
            data += ["{}={}".format(key, val(kwargs[key]))]

    return get_server_url(path)+"?"+"&".join(data)


def make_request(method, url, data=None):
    request_method = {
        "get": requests.get,
        "put": requests.put,
        "post": requests.post,
        "patch": requests.patch,
        "delete": requests.delete
    }[method.lower()]

    if data is None:
        return request_method(url, auth=get_auth())

    if isinstance(data, Serializable):
        return request_method(url, json=data.serialize(), auth=get_auth())
    else:
        return request_method(url, json=data, auth=get_auth())
