from json import JSONDecodeError

from luna.serializer import Serializable
from datetime import datetime


DATETIME_FORMAT = "%Y:%m:%d %H:%M:%S"


class Rect(Serializable):
    def __init__(self, x, y, width, height):
        self.x = x
        self.y = y
        self.width = width
        self.height = height

    @staticmethod
    def read_from_json(json):
        fields = ["x", "y", "width", "height"]
        return Rect.read_instance_from_json(fields, json)


class Detection(Serializable):
    def __init__(self, rect, yaw, pitch, roll, score):
        self.rect = rect
        self.yaw = yaw
        self.pitch = pitch
        self.roll = roll
        self.score = score

    @staticmethod
    def read_from_json(json):
        fields = [("rect", Rect), "yaw", "pitch", "roll", "score"]
        return Detection.read_instance_from_json(fields, json)


class FeatureSet(Serializable):
    def __init__(self, data):
        self.data = data

    @staticmethod
    def read_from_json(json):
        return FeatureSet.read_instance_from_json(["data"], json)


class Descriptor(Serializable):
    def __init__(self, data):
        self.data = data

    @staticmethod
    def read_from_json(json):
        return Descriptor.read_instance_from_json(["data"], json)


class LatLng(Serializable):
    def __init__(self, latitude, longitude):
        self.latitude = latitude
        self.longitude = longitude

    @staticmethod
    def read_from_json(json):
        fields = ["latitude", "longitude"]
        return LatLng.read_instance_from_json(fields, json)


class ExIf(Serializable):
    def __init__(self, make, model, dateTime, orientation, artist, software,
                 digitalZoomRatio, flash, uid, gps):
        self.make = make
        self.model = model
        self.dateTime = dateTime
        self.orientation = orientation
        self.artist = artist
        self.software = software
        self.digitalZoomRatio = digitalZoomRatio
        self.flash = flash
        self.uid = uid
        self.gps = gps

    @property
    def date_time(self):
        return datetime.strptime(self.dateTime, DATETIME_FORMAT)

    @staticmethod
    def read_from_json(json):
        fields = ["make", "model", "dateTime", "orientation", "artist", "software",
                  "digitalZoomRatio", "flash", "uid", ("gps", LatLng)]
        return ExIf.read_instance_from_json(fields, json)


class TemplateInput(Serializable):
    def __init__(self, image, bare=False, detection=None, featureSet=None,
                 descriptor=None):
        self.image = image
        self.bare = bare
        self.detection = detection
        self.featureSet = featureSet
        self.descriptor = descriptor

    @property
    def feature_set(self):
        return self.featureSet

    @staticmethod
    def read_from_json(json):
        fields = [
            "image",
            "bare",
            ("detection", Detection),
            ("featureSet", FeatureSet),
            ("descriptor", Descriptor),
        ]
        return TemplateInput.read_instance_from_json(fields, json)


class TemplateOutput(Serializable):
    def __init__(self, id, rect, rectISO, yaw, pitch, roll, score, quality, exif):
        self.id = id
        self.yaw = yaw
        self.pitch = pitch
        self.roll = roll
        self.score = score
        self.quality = quality
        self.rect = rect
        self.rectISO = rectISO
        self.exif = exif

    @staticmethod
    def read_from_json(json):
        fields = [
            "id",
            "yaw",
            "pitch",
            "roll",
            "score",
            "quality",
            ("rect", Rect),
            ("rectISO", Rect),
            ("exif", ExIf),
        ]
        return TemplateOutput.read_instance_from_json(fields, json)


class SimilarTemplate(Serializable):
    def __init__(self, id, similarity):
        self.id = id
        self.similarity = similarity

    @staticmethod
    def read_from_json(json):
        fields = ["id", "similarity"]
        return SimilarTemplate.read_instance_from_json(fields, json)


class SimilarTemplates(Serializable):
    def __init__(self, reference, matches):
        self.reference = reference
        self.matches = matches

    @staticmethod
    def read_from_json(json):
        fields = [
            "reference",
            ("matches", list, SimilarTemplate)
        ]
        return SimilarTemplates.read_instance_from_json(fields, json)


class AsyncOutput(Serializable):
    def __init__(self, requestId):
        self.requestId = requestId

    @staticmethod
    def read_from_json(json):
        fields = ["requestId"]
        return AsyncOutput.read_instance_from_json(fields, json)


class Error(Serializable):
    def __init__(self, desc, detail, error):
        self.desc = desc
        self.detail = detail
        self.error = error

    @staticmethod
    def read_from_json(json):
        fields = ["desc", "detail", "error"]
        return Error.read_instance_from_json(fields, json)


class Result:
    def __init__(self, response, kls=None):
        self.status_code = response.status_code

        if 200 <= self.status_code <= 300:
            if kls is None:
                return

            self.data = kls.read_from_json(response.json())
        else:
            try:
                self.data = Error.read_from_json(response.json())
            except JSONDecodeError:
                self.data = Error(desc=response.text, detail="", error=response.status_code)
