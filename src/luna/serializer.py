class Serializable:
    def serialize(self):
        json = {}

        for p in self.__dict__:
            if isinstance(self.__dict__[p], list):
                json[p] = []
                for item in self.__dict__[p]:
                    json[p].append(item.serialize())
            elif isinstance(self.__dict__[p], Serializable):
                json[p] = self.__dict__[p].serialize()
            else:
                json[p] = self.__dict__[p]

        return json

    @staticmethod
    def read_from_json(json):
        pass

    # TODO: Отрефакторить, сликом много вложенностей, сложный для понимания код
    @classmethod
    def read_instance_from_json(kls, fields, json):
        data = {}

        for field in fields:
            if json is None:
                data[field] = None
                continue

            if isinstance(field, tuple):
                if len(field) == 3:
                    key, instance_type, _ = field
                else:
                    key, instance_type = field

                if instance_type is None:
                    data[key] = None
                    continue

                if instance_type == list:
                    lst = json.get(key)
                    deserialized_lst = []

                    for item in lst:
                        instance_type = field[2] if len(field) >= 2 else None

                        if instance_type is not None:
                            deserialized_lst.append(instance_type.read_from_json(item))
                        else:
                            deserialized_lst.append(item)

                    data[key] = deserialized_lst
                else:
                    data[key] = instance_type.read_from_json(json.get(key))
            elif isinstance(json.get(field), list):
                data[field] = []
                for item in json.get(field):
                    data[field] = item
            else:
                data[field] = json.get(field)

        return kls(**data)
