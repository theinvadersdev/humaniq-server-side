import os
from django.test import TestCase

from luna.data import *
from luna.api import *


class DataTest(TestCase):
    def test_serialize(self):
        rect_json = {
            "x": 1,
            "y": 2,
            "width": 3,
            "height": 4,
        }

        rect = Rect.read_from_json(rect_json)
        self.assertEqual(rect.x, 1)
        self.assertEqual(rect.y, 2)
        self.assertEqual(rect.width, 3)
        self.assertEqual(rect.height, 4)

        rect_json = rect.serialize()
        self.assertEqual(rect_json["x"], 1)
        self.assertEqual(rect_json["y"], 2)
        self.assertEqual(rect_json["width"], 3)
        self.assertEqual(rect_json["height"], 4)

        detection_json = {
            "rect": rect_json,
            "yaw": 1,
            "pitch": 2,
            "roll": 3,
            "score": 4
        }

        detection = Detection.read_from_json(detection_json)
        self.assertEqual(detection.rect.x, 1)
        self.assertEqual(detection.rect.y, 2)
        self.assertEqual(detection.rect.width, 3)
        self.assertEqual(detection.rect.height, 4)

        self.assertEqual(detection.yaw, 1)
        self.assertEqual(detection.pitch, 2)
        self.assertEqual(detection.roll, 3)
        self.assertEqual(detection.score, 4)

        t1 = SimilarTemplate(0, ":)")
        t2 = SimilarTemplate(1, ":/")

        json = SimilarTemplates("123", [t1, t2]).serialize()
        templates = SimilarTemplates.read_from_json(json)

        self.assertEqual(templates.matches[0].id, 0)
        self.assertEqual(templates.matches[1].id, 1)

        self.assertEqual(templates.matches[0].similarity, ":)")
        self.assertEqual(templates.matches[1].similarity, ":/")


class ApiTest(TestCase):
    face = ""

    def load_face(self, file_name):
        with open(os.path.join(settings.STATIC_ROOT, "tests", file_name), "r") as f:
            self.face = f.read()

    def test_templates(self):
        data = TemplateInput(image=self.face)
        res = templates(data)
        self.assertEqual(res.status_code, 400)
        print(res.data.serialize())

        res = templates(data, id=1)
        self.assertEqual(res.status_code, 400)
        print(res.data.serialize())

        self.load_face("test_photo.txt")
        data = TemplateInput(image=self.face)
        res = templates(data, id=1)
        print(res.data.serialize())
        self.assertEqual(res.status_code, TEMPLATES_OK_STATUS_CODE)

        #

        self.load_face("test_photo_3.txt")
        data = TemplateInput(image=self.face)
        res = templates(data, id=3)
        print(res.data.serialize())
        self.assertEqual(res.status_code, TEMPLATES_OK_STATUS_CODE)
        #

        res = put_lists(id="test", descriptors=[1, 3])

        if res.status_code >= 400:
            print(res.data.serialize())

        self.assertEqual(res.status_code, 200)
        res = similar_templates(id=1, candidates_list="test")
        print(res.data.serialize())
        self.assertEqual(res.status_code, 200)

        # Поиск образа
        self.load_face("test_photo_2.txt")
        data = TemplateInput(image=self.face)
        res = templates(data, id=2)
        print(res.data.serialize())
        self.assertEqual(res.status_code, TEMPLATES_OK_STATUS_CODE)

        res = similar_templates(id=2, candidates_list="test")
        print(res.data.serialize())
        self.assertEqual(res.status_code, 200)
